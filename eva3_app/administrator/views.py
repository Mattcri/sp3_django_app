from django.shortcuts import render, redirect
from .forms import SignUpForm, LoginForm
from django.contrib.auth import authenticate, login, logout
from .decorators import admin_permission, client_permission, layer_permission, unauthenticated_user
from django.contrib.auth.decorators import login_required
from online.models import Question
from .models import User

# Create your views here.

@unauthenticated_user
def indexView(request):
  return render(request, 'index.html')

def registerView(request):
  msg = None
  if request.method == 'POST':
    form = SignUpForm(request.POST)
    if form.is_valid():
      user = form.save()
      msg = 'Usuario creado'
      return redirect('login')
    else:
      msg = 'Datos no validos'
  
  else:
    form = SignUpForm()
  
  return render(request, 'register.html', { 'form': form, 'msg': msg })

@unauthenticated_user
def loginView(request):

  form = LoginForm(request.POST or None)
  msg = None
  if request.method == 'POST':
    if form.is_valid():
      username = form.cleaned_data.get('username')
      password = form.cleaned_data.get('password')
      user = authenticate(username=username, password=password)
      if user is not None and user.is_admin:
        login(request, user)
        return redirect('home_admin')
      elif user is not None and user.is_client:
        login(request, user)
        return redirect('home_client')
      elif user is not None and user.is_layer:
        login(request, user)
        return redirect('home_layer')
      else:
        msg = 'Credenciales Invalidas'
    else:
      msg = 'Error en la validación de tus datos'
    
  return render(request, 'login.html', { 'form': form, 'msg': msg })

def logOutUser(request):
  logout(request)
  return redirect('home')

@login_required
@admin_permission
def homeAdmin(request):
  totalQuestions = Question.objects.count()
  totalPending = Question.objects.filter(status="Pendiente").count()
  totalReply = Question.objects.filter(status="Respondida").count()
  questionsList = Question.objects.all()
  usersList = User.objects.filter(is_staff=False)

  context = {
    'totalQ': totalQuestions,
    'totalP': totalPending,
    'totalR': totalReply,
    'questions': questionsList,
    'users': usersList
  }
  return render(request, 'homeAdmin.html', context)

@login_required
@admin_permission
def deleteQuestion(request, pk):
  question = Question.objects.get(id=pk)
  question.delete()
  return redirect('home_admin')

@login_required
@admin_permission
def deleteUser(request, pk):
  user = User.objects.get(id=pk)
  user.delete()
  return redirect('home_admin')
# @login_required
# @client_permission
# def homeClient(request):
#   return render(request, 'homeClient.html')

