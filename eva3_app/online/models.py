from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Question(models.Model):
  STATUS = (
    ('Pendiente', 'Pendiente'),
    ('Respondida', 'Respondida')
  )
  nameClient = models.CharField(max_length=150, null=True)
  emailClient = models.EmailField(max_length=150, null=True)
  subject = models.CharField(max_length=150, null=True)
  status = models.CharField(max_length=180, null=True, choices=STATUS)

  class Meta:
    db_table = 'question'

  def __str__(self):
    return '{} by {}'.format(self.subject, self.nameClient)

class Answer(models.Model):
  nameLayer = models.CharField(max_length=150, null=True)
  emailLayer = models.EmailField(max_length=150, null=True)
  question = models.ForeignKey(Question, null=True, on_delete=models.CASCADE)
  response = models.TextField(max_length=600, null=True)

  class Meta:
    db_table = 'answer'

  def __str__(self):
    return '{} by {}'.format(self.response, self.nameLayer)





