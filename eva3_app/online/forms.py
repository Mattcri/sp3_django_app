from django.forms import ModelForm, TextInput, Select, EmailInput, Textarea
from .models import Question, Answer

class createQuestion(ModelForm):
  class Meta:
    model = Question
    fields = '__all__'
    widgets = {
      'nameClient': TextInput(attrs={
        "class": "form-control",
        "readonly": "readonly",
        "id": "nameClient"
      }),
      'emailClient': EmailInput(attrs={
        "class": "form-control",
        "readonly": "readonly",
        "id": "emailClient"
      }),
      'subject': TextInput(attrs={
        "class": "form-control",
        "id": "subject"
      }),
      'status': Select(attrs={
        "class": "form-select",
        "id": "status"
      })
    }

class sendAnswer(ModelForm):
  class Meta:
    model = Answer
    fields = ['nameLayer', 'emailLayer', 'response']
    widgets = {
      'nameLayer': TextInput(attrs={
        "class": "form-control",
        "id": "nameLayer",
        "readonly": "readonly"
      }),
      'emailLayer': EmailInput(attrs={
        "class": "form-control",
        "id": "emailLayer",
        "readonly": "readonly"
      }),
      'response': Textarea(attrs={
        "class": "form-control",
        "rows": "3"
      })
    }