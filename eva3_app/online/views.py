from django.shortcuts import render, redirect
from administrator.decorators import client_permission, layer_permission, admin_permission
from django.contrib.auth.decorators import login_required
from .forms import createQuestion, sendAnswer
from .models import Question, Answer


# Create your views here.
@login_required
@client_permission
def homeClient(request):
  
  dataForm = postForm(request)
  dataAnswers = getAnswerData()
  pendingQuestions = Question.objects.filter(status="Pendiente")
  print('ID profile',request.user.id)
  return render(request, 'homeClient.html', { 'form': dataForm, 'answers': dataAnswers, 'questions': pendingQuestions } )

def postForm(request):
  init_data = {
    'nameClient': request.user.username,
    'emailClient': request.user.email
  }
  form = createQuestion(initial=init_data)
  if request.method == 'POST':
    form = createQuestion(request.POST)
    if form.is_valid():
      form.save()
      form = createQuestion(initial=init_data)
  
  context = form
  return context

def getAnswerData():
  getAnswers = Answer.objects.all()
  context = getAnswers
  return context

@login_required
@client_permission
def answerDetail (request, pk):
  answer = Answer.objects.get(id=pk)
  context = { 'answer': answer }
  return render(request, 'answerDetails.html', context)

@login_required
@layer_permission
def homeLayer(request):
  getQuestions = Question.objects.all()
  context = {'question': getQuestions}
  return render(request, 'homeLayer.html', context)

@login_required
@layer_permission
def answerLayer(request, pk):
  init_data = {
    'nameLayer': request.user.username,
    'emailLayer': request.user.email
  }
  form = sendAnswer(initial=init_data)
  question = Question.objects.get(id=pk)
  # question_id = question.id
  if request.method == 'POST':
    form = sendAnswer(request.POST)
    if form.is_valid():
      # Para poder enviar un valor por defecto o cambiar el valor de un campo en el formulario se debe crear una nueva instancia, ya que será un objeto distinto de form por eso creamos la instancia de obj
      obj = form.save(commit=False)
      obj.question_id = question.id
      question.status = 'Respondida'
      obj.save()
      question.save()
      return redirect('home_layer')

  print(question.status)
  print(question.subject)
  print(question.id)
  
  # questionFields = question.order_set.all()
  context = { 'question': question, 'form': form }
  return render(request, 'answerQuestion.html', context)

