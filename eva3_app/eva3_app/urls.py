"""eva3_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from administrator.views import loginView, logOutUser, registerView, indexView, homeAdmin, deleteQuestion, deleteUser
from online.views import homeClient, homeLayer, answerLayer, answerDetail

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', indexView, name="home"),
    path('login/', loginView, name="login" ),
    path('logout/', logOutUser, name="logout"),
    path('register/', registerView, name="register"),
    path('administrator/', homeAdmin, name="home_admin"),
    path('client/', homeClient, name="home_client"),
    path('layer/', homeLayer, name="home_layer"),
    path('layer/answer/<str:pk>/', answerLayer,name='answer_layer'),
    path('client/answer/<str:pk>/', answerDetail, name='answer_detail'),
    path('administrator/delete/<str:pk>', deleteQuestion, name="delete_question"),
    path('administrator/delete/user/<str:pk>', deleteUser, name="delete_user"),
]
